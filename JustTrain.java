package innerclasses;

/**
 * Created by a on 30.07.2015.
 */
class Hui1{
    Hui1(){
        System.out.println("Hui1");
    }
}
public class JustTrain {
    public static void main(String[] args) {
        new Hui1(){
            {
                System.out.println("JustTrain");
            }
        };
    }
}
